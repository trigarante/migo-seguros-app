import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '~/environment/environment';

@Injectable({ providedIn: 'root' })
export class PasswordRecoveryService {

    constructor(
        private http: HttpClient
    ) { }

    emailVerificationExist(email: string) {
        return this.http.get(`${environment.apiMark}/auth-cliente/get-by-email/${email}`);
    }

    sendEmailAfterVerification(clientID: number) {
        const brandID: number = 1;
        return this.http.get(`${environment.apiMark}/v1/apps/recuperar-email/${clientID}/${brandID}`);
    }

}