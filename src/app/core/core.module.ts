import { NgModule } from '@angular/core';
import { AuthService } from './Services/Auth/Auth.service';
import { DataClientService } from './Services/DataClient/dataclient.service';
import { PoliciesService } from './Services/Policies/policies.service';

@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [
        AuthService,
        DataClientService,
        PoliciesService
    ],
})
export class CoreModule { }
