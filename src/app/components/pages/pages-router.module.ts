import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from '@nativescript/angular';
import { LogoutComponent } from '../logout/components/logout.component';
import { EventOfClaimComponent } from './event-of-claim/components/event-of-claim.component';
import { HomeComponent } from './home/components/home/home.component';
import { OfficialIdentificationComponent } from './official-identification/components/official-identification.component';
import { PetitionsComponent } from './petitions/components/petitions.component';
import { PoliciesComponent } from './policies/components/policies/policies.component';
import { ProfileComponent } from './profile/components/profile.component';
import { SettingsComponent } from './settings/components/settings/settings.component';
import { SupportComponent } from './support/components/support/support.component';
import { VehicleInspectionComponent } from './vehicle-inspection/components/vehicle-inspection.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'policies',
        component: PoliciesComponent
    },
    {
        path: 'official-identification',
        component: OfficialIdentificationComponent
    },
    {
        path: 'vehicle-inspection',
        component: VehicleInspectionComponent
    },
    {
        path: 'event-of-claim',
        component: EventOfClaimComponent
    },
    {
        path: 'profile',
        component: ProfileComponent
    },
    {
        path: 'settings',
        component: SettingsComponent
    },
    {
        path: 'support',
        component: SupportComponent
    },
    {
        path: 'logout',
        component: LogoutComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PagesRoutingModule { }
