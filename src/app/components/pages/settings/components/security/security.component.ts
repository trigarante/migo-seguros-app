import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { isAndroid, isIOS, Page } from '@nativescript/core';

@Component({
    selector: 'app-settings-security',
    templateUrl: 'security.component.html',
    styleUrls: ['security.component.scss']
})

export class SettingsSecurityComponent implements OnInit {
    
    titleActionBar: string = 'Seguridad';
    securityOptions: Array<any> = [];

    isIOS: any;
    isAndroid: any;
    
    constructor(
        private routerExtensions: RouterExtensions
    ) {
        this.isIOS = isIOS;
        this.isAndroid = isAndroid;
    }

    ngOnInit() {
        this.securityOptions = [
            {
                id: 0,
                icon: String.fromCharCode(0xf502),
                title: 'Cambiar contraseña',
                url: '/settings/security/password'
            }
        ]
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    onItemTap(url: string) {
        this.routerExtensions.navigate([url], {
            transition: {
                name: 'slide'
            }
        })
    }
}