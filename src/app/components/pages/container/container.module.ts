import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { NativeScriptUISideDrawerModule } from 'nativescript-ui-sidedrawer/angular';
import { AppRoutingModule } from '~/app/app-routing.module';
import { ContainerComponent } from './components/container.component';
import { ContainerRoutingModule } from './container-routing.module';


@NgModule({
    imports: [
        AppRoutingModule,
        NativeScriptCommonModule,
        NativeScriptUISideDrawerModule,
        ContainerRoutingModule
    ],
    exports: [],
    declarations: [
        ContainerComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class ContainerModule { }
