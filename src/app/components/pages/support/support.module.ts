import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { ConcentratorAccountComponent } from './components/accounts/concentratoraccount.component';
import { GeneralConditionsComponent } from './components/general-conditions/general-conditions.component';
import { ViewDocumentComponent } from './components/view-document/view-document.component';

@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    exports: [],
    declarations: [
        ConcentratorAccountComponent,
        ViewDocumentComponent,
        GeneralConditionsComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SupportModule { }
