import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { CotizadorComponent } from './components/cotizador.component';
import { CotizadorRoutingModule } from './cotizador-routing.module';


@NgModule({
    imports: [
        NativeScriptCommonModule,
        CotizadorRoutingModule
    ],
    declarations: [CotizadorComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class CotizadorModule { }
